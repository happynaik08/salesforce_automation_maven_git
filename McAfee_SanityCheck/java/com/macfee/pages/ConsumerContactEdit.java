package com.macfee.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import com.mcafee.framework.AutomationHelper;

/**
*
* @author Puneeth & Balamurugan
*
*/

public class ConsumerContactEdit 
{
	private WebDriver driver;
	private By sfSalutationDDField=By.id("name_salutationacc2");
	private By sfFirstNameTxtField=By.id("name_firstacc2");
	private By sfLastNameTxtField=By.id("name_lastacc2");
	private By sfPrimaryEmaileTxtField=By.id("PersonEmail");
	private By sfPrimaryPhNoTxtField=By.id("acc10");
	private By sfLanguageDDField=By.id("00N3600000QVLYJ");
	private By sfCountryDDField=By.id("00N3600000QVLYG");
	private By sfInputErrorMsg=By.xpath(".//input[@class='error']");
	private By sfSelectErrorMsg=By.xpath(".//select[@class='error']");
	private By sfSaveBtnField=By.xpath(".//input[@title = 'Save']");
	
	ConsumerContactEdit(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public WebElement getsalutationDDField()
	{
		return AutomationHelper.findElement(driver, sfSalutationDDField, 10);
	}
	
	public WebElement getsFirstNameTxtField()
	{
		return AutomationHelper.findElement(driver, sfFirstNameTxtField, 10);
	}

	public WebElement getLastNameTxtField()
	{
		return AutomationHelper.findElement(driver, sfLastNameTxtField, 10);
	}
	
	public WebElement getPrimaryEmaileTxtField()
	{
		return AutomationHelper.findElement(driver, sfPrimaryEmaileTxtField, 10);
	}
	
	public WebElement getPrimaryPhNoTxtField()
	{
		return AutomationHelper.findElement(driver, sfPrimaryPhNoTxtField, 10);
	}
	
	public WebElement getLanguageDDField()
	{
		return AutomationHelper.findElement(driver, sfLanguageDDField, 10);
	}
	
	public WebElement getCountryDDField()
	{
		return AutomationHelper.findElement(driver, sfCountryDDField, 10);
	}
	
	public WebElement getInputErrorMsg()
	{
		return AutomationHelper.findElement(driver, sfInputErrorMsg, 10);
	}
	
	public WebElement getSelectErrorMsg()
	{
		return AutomationHelper.findElement(driver, sfSelectErrorMsg, 10);
	}
	
	public WebElement getsfSaveBtnField()
	{
		return AutomationHelper.findElement(driver, sfSaveBtnField, 10);
	}
	
	public void selectSalutation(String salutation)
	{
		Select dropdown= new Select(getsalutationDDField()); 
		dropdown.selectByVisibleText(salutation); 
		System.out.println("Customer Salutation is " + salutation);
	}
	
	public void putFirstName(String firtname)
	{
		getsFirstNameTxtField().sendKeys(firtname);
		System.out.println("Customer First Name is " + firtname);
	}
	
	public void putLastName(String lastname)
	{
		getLastNameTxtField().sendKeys(lastname);
		System.out.println("Customer Last Name is " + lastname);
	}
	
	public void putPrimaryEmailID(String primaryemail)
	{
		getPrimaryEmaileTxtField().sendKeys(primaryemail);
		System.out.println("Customer Primary EmailID is " + primaryemail);
	}
	
	public void putPrimaryPhNo(String primaryphone)
	{
		getPrimaryPhNoTxtField().sendKeys(primaryphone);
		System.out.println("Customer Primary Phone No is " + primaryphone);
	}
	
	public void selectCustLanguage(String language)
	{
		Select dropdown= new Select(getLanguageDDField()); 
		dropdown.selectByVisibleText(language); 
		System.out.println("Customer Language is " + language);
	}
	
	public void selectCustCountry(String country)
	{
		Select dropdown= new Select(getCountryDDField()); 
		dropdown.selectByVisibleText(country); 
		System.out.println("Customer Country is " + country);
	}
	
	public List<String> mandatoryFieldsForConsumerContact()
	{
		List<String> mandatoryFieldValues = new ArrayList<String>();
		List<WebElement> inputErrorMsg = driver.findElements(By.xpath(".//input[@class='error']"));
		List<WebElement> selectErrorMsg = driver.findElements(By.xpath(".//select[@class='error']"));
		inputErrorMsg.addAll(selectErrorMsg);
		System.out.println("The no of mandatory fields = " + inputErrorMsg.size());
		for(WebElement mandatoryFields:inputErrorMsg)
		{
			System.out.println(driver.findElement(By.xpath(".//label[@for ='" + mandatoryFields.getAttribute("id") + "']")).getText());
			mandatoryFieldValues.add(driver.findElement(By.xpath(".//label[@for ='" + mandatoryFields.getAttribute("id") + "']")).getText());
		}
		return mandatoryFieldValues;
	}
	
	public ConsumerContactEdit clickSave() 
	{
		getsfSaveBtnField().click();
		//Assert.assertEquals(getEmailIleinnerTxtField().getText(), primaryEmailID);
		System.out.println("Clicked Save Button");
		return new ConsumerContactEdit(driver);
	}
}
