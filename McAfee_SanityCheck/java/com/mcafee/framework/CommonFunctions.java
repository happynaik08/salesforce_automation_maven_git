package com.mcafee.framework;

import org.openqa.selenium.WebDriver;
import com.macfee.pages.LoginPage;

/**
*
* @author Balamurugan
*
*/

public class CommonFunctions 
{
	private WebDriver driver;

	public CommonFunctions(WebDriver driver) 
	{	
		this.driver=driver;
	}
	
	public LoginPage launchSFApplication()
	{
		System.out.println("URL");
		driver.get(ConfigData.SFURL);
		return new LoginPage(driver);
	}
	
	public WebDriver switchFrame(String id)
	{
		return driver.switchTo().frame(id);
	}
}
