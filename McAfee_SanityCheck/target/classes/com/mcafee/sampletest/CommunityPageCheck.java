/**
 * 
 */
package com.mcafee.sampletest;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.mcafee.pages.CommunityPage;

/**
 * @author veenashr
 *
 */
public class CommunityPageCheck {
	
	private WebDriver driver;
	
	public CommunityPageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void ClickandVerifyCommunity()
	{
		CommunityPage element=new CommunityPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement community;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		community= wait.until(ExpectedConditions.visibilityOf(element.getCommunity()));
		community.click();
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getCommunityElement().isDisplayed());
		driver.switchTo().window(winHandleBefore);
		
	}

	public void VerifyBlogs()
	{
		CommunityPage element=new CommunityPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement blogs;  
		
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		blogs= wait.until(ExpectedConditions.visibilityOf(element.getBlogs()));
		blogs.click();
		
		String winHandleBefore2 = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		String title="Securing Tomorrow. Today. | McAfee Blogs";
		
		if(driver.getTitle().contains(title))
		{
			System.out.println("blogs page loaded fine");
			
		}
		
		else
			System.out.println("blogs page failed");
		
	
		
		driver.close();
		driver.switchTo().window(winHandleBefore2);
		driver.switchTo().window(winHandleBefore);
		
	}
	
	public void VerifyEvents()
	{
		CommunityPage element=new CommunityPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement events;  
		
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		events= wait.until(ExpectedConditions.visibilityOf(element.getEvents()));
		events.click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true,element.getEventsElement().isDisplayed());
		driver.switchTo().window(winHandleBefore);
	}
	
	public void VerifyWebinars()
	{
		CommunityPage element=new CommunityPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement webinars; 
		
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		webinars= wait.until(ExpectedConditions.visibilityOf(element.getWebinars()));
		webinars.click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getWebinarsElement().isDisplayed());
		driver.switchTo().window(winHandleBefore);
	}
	
}
