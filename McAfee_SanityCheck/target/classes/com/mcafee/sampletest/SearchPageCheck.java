/**
 * 
 */
package com.mcafee.sampletest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.mcafee.pages.SearchPage;

/**
 * @author veenashr
 *
 */
public class SearchPageCheck 
{
private WebDriver driver;
	
	public SearchPageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void SearchTest1()
	{
		SearchPage element=new SearchPage(driver);
		element.getSearch().click();
		element.getSearchTextbox().clear();
		element.getSearchTextbox().sendKeys("partner");
		element.getSearchTextbox().sendKeys(Keys.ENTER);
		Assert.assertEquals(true,element.getSearchResult().isDisplayed());
	
		
		
		
		
	}
	
	public void VerfifySearchTest1()
	{	
		SearchPage element=new SearchPage(driver);
		element.getSearchResult().click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		Assert.assertEquals(true, driver.getTitle().toLowerCase().contains("partner"));
	
	}
	
	
	public void SearchTest2()
	{
		SearchPage element=new SearchPage(driver);
		element.getSearch().click();
		element.getSearchTextbox().clear();
		element.getSearchTextbox().sendKeys("threat");
		element.getSearchTextbox().sendKeys(Keys.ENTER);
		Assert.assertEquals(true, element.getSearchResult().isDisplayed());
	}
	
	public void VerifySearchTest2()
	{
		SearchPage element=new SearchPage(driver);
		element.getSearchResult().click();
		driver.manage().timeouts().implicitlyWait(5000, TimeUnit.SECONDS);
		
		Assert.assertEquals(true, driver.getTitle().toLowerCase().contains("threat"));
	}
	
	public void Navigationtest() throws Exception
	{
		SearchPage element=new SearchPage(driver);
		element.getLastPage().click();
		Thread.sleep(3000);
		Assert.assertEquals(true,element.getSearchResult().isDisplayed());
		
		
		element.getPrevPage().click();
		Thread.sleep(3000);
		Assert.assertEquals(true,element.getSearchResult().isDisplayed());
		

		element.getFirstPage().click();
		Thread.sleep(3000);
		Assert.assertEquals(true,element.getSearchResult().isDisplayed());
	
		element.getNextPage().click();
		Thread.sleep(3000);
		Assert.assertEquals(true,element.getSearchResult().isDisplayed());
		
	}
	
	
}
