/**
 * 
 */
package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;

/**
 * @author veenashr
 *
 */
public class ProductsPage {
	
	private WebDriver driver;
	PropertiesReader config=new PropertiesReader(); 

	
	public ProductsPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public WebElement getProducts()
	{
		return driver.findElement(By.xpath(config.getProducts()));
	}
	
	public WebElement getProductsElement()
	{
		return driver.findElement(By.xpath(config.getProductsElement()));
	}
	
	public WebElement getAllProducts()
	{
		return driver.findElement(By.xpath(config.getAllProducts()));
	}
	
	public WebElement getAllProductsElement()
	{
		return driver.findElement(By.xpath(config.getAllProductsElement()));
	}
	
	public WebElement getEndpointprotectionSuite()
	{
		return driver.findElement(By.xpath(config.getEndpointProtectionSuite()));
	}
	
	public WebElement getEndpointElement()
	{
		return driver.findElement(By.xpath(config.getEndpointElement()));
	}
	
	public WebElement getDatacenterSecurity()
	{
		return driver.findElement(By.xpath(config.getDataCenterSecurity()));
	}
	
	public WebElement getDatacenterSecurityElement()
	{
		return driver.findElement(By.xpath(config.getDataCentreElement()));
	}
	
	public WebElement getWebProtection()
	{
		return driver.findElement(By.xpath(config.getWebProtection()));
	}
	
	public WebElement getWebProtectionElement()
	{
		return driver.findElement(By.xpath(config.getWebProtectionElement()));
	}
	

}
