/**
 * 
 */
package com.mcafee.utility;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.mcafee.pages.Constant_variables;

import testingTestNG.CommunityPageTest;
import testingTestNG.HomePageTest;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author veenashr
 *
 */
public class SendEmail {
	

	 
		public static void sendemail() {
	 
			// Create object of Property file
			Properties props = new Properties();
	 
			// this will set host of server- you can change based on your requirement 
			props.put("mail.smtp.host", "10.44.93.54");
			// OutlookBG.intel.com 		 smtp.gmail.com
	 
			//set the port of socket factory 
			props.put("mail.smtp.socketFactory.port", "25");
	 
			// set socket factory
			props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
	 
			// set the authentication to true
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable","true");
	 
			// set the port of SMTP server
			props.put("mail.smtp.port", "25");//587 instead of 25
	 
			// This will handle the complete authentication
			Session session = Session.getDefaultInstance(props,
	 
					new javax.mail.Authenticator() {
	 
						protected PasswordAuthentication getPasswordAuthentication() {
	 
						return new PasswordAuthentication("veenashree_p@mcafee.com", "i<3india");
	 
						}
	 
					});
	 
			try {
	 
				// Create object of MimeMessage class
				Message message = new MimeMessage(session);
	 
				// Set the from address
				message.setFrom(new InternetAddress("veenashree_p@mcafee.com"));
	 
				// Set the recipient address
				message.setRecipients(Message.RecipientType.TO,InternetAddress.parse("veenashree_p@mcafee.com"));
	            
	                        // Add the subject link
				message.setSubject("mcafee.com test report");
	 
				// Create object to add multimedia type content
				BodyPart messageBodyPart1 = new MimeBodyPart();
	 
				// Set the body of email
				messageBodyPart1.setText("Please find the attachment");
	 
				// Create another object to add another content
				MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			//	MimeBodyPart messageBodyPart3 = new MimeBodyPart();
				
				
				  String Report_zip=null;
				 
					String env_name=Constant_variables.env.toString();
					if(env_name.equalsIgnoreCase("mcafee-prod"))
					{
					Report_zip="Report_prod";
					
					}
					else if(env_name.equalsIgnoreCase("mcafee-dr"))
					{
						Report_zip="Report_dr";
						
					}
	 
				// Mention the file which you want to send
				String filename =  System.getProperty("user.dir")+"\\"+Report_zip+".zip";
				
			
	 
				// Create data source and pass the filename
				DataSource source = new FileDataSource(filename);
			//	DataSource source1 = new FileDataSource(filename1);
	 
				// set the handler
				messageBodyPart2.setDataHandler(new DataHandler(source));
			//	messageBodyPart3.setDataHandler(new DataHandler(source1));
	 
				// set the file
				String fileName = new File(filename).getName();
				
				messageBodyPart2.setFileName(fileName);
			//	messageBodyPart3.setFileName(filename1);
	 
				// Create object of MimeMultipart class
				Multipart multipart = new MimeMultipart();
	 
				// add body part 1
				multipart.addBodyPart(messageBodyPart2);
	 
				// add body part 2
				multipart.addBodyPart(messageBodyPart1);
				
			//	multipart.addBodyPart(messageBodyPart3);
				
	 
				// set the content
				message.setContent(multipart);
	 
				// finally send the email
				Transport.send(message);
	 
				System.out.println("=====Email Sent=====");
	 
			} catch (MessagingException e) {
	 
				throw new RuntimeException(e);
	 
			}
	 
		}
		
		
		 public static void zip(){
			   try
			   {
				   String Report_zip=null;
				   String Report_path=null;
				   
					String env_name=Constant_variables.env.toString();
					if(env_name.equalsIgnoreCase("mcafee-prod"))
					{
					Report_zip="Report_prod";
					Report_path="Reports-prod";
					}
					else if(env_name.equalsIgnoreCase("mcafee-dr"))
					{
						Report_zip="Report_dr";
						Report_path="Reports-dr";
					}
			    File inputFolder=new File(System.getProperty("user.dir")+"\\"+Report_path);
			    File outputFolder=new File(System.getProperty("user.dir")+"\\"+Report_zip+".zip");
			    ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outputFolder)));
			    BufferedInputStream in = null;
			    byte[] data  = new byte[1000];
			    String files[] = inputFolder.list();
			    for (int j=0; j<files.length; j++)
			    {
			     in = new BufferedInputStream(new FileInputStream
			     (inputFolder.getPath() + "/" + files[j]), 1000); 
			     out.putNextEntry(new ZipEntry(files[j]));
			     int totalcount;
			     while((totalcount= in.read(data,0,1000)) != -1)
			     {
			      out.write(data, 0, totalcount);
			     }
			     out.closeEntry();
			  }
			  out.flush();
			  out.close();  
			}
			  catch(Exception e)
			  {
			   e.printStackTrace();
			           
			  }
			 }
		
		
	 
	}
	
	


