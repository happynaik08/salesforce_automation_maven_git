package testingTestNG;

import org.junit.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class SingleLogTest extends BaseExample {
	    @Test
	    public void passTest() {
	        test = extent.startTest("passTest");
	        test.log(LogStatus.PASS, "Pass");
	        
	        Assert.assertEquals(test.getRunStatus(), LogStatus.PASS);
	    }
	    
	    @Test
	    public void intentionalFailure() throws Exception {
	        test = extent.startTest("intentionalFailure");
	        throw new Exception("intentional failure");        
	    }
	}


	