/**
 * 
 */
package com.mcafee.sampletest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.mcafee.pages.ServicesPage;

/**
 * @author veenashr
 *
 */
public class ServicesPageCheck 
{
	private WebDriver driver;
	
	public ServicesPageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void ServicesCheck()
	{
		ServicesPage element=new ServicesPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement services;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		services= wait.until(ExpectedConditions.visibilityOf(element.getServices()));
		services.click();
		
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getServicesElement().isDisplayed());
		driver.switchTo().window(winHandleBefore);
	}

	public void FoundstoneCheck()
	{
		ServicesPage element=new ServicesPage(driver);
		 JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("window.scrollBy(0,1000)", "");
		element.getFoundstoneServices().click();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getFoundstoneElement().isDisplayed());
		
	}
	
	public void SolutionCheck()
	{
		ServicesPage element=new ServicesPage(driver);
		 JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("window.scrollBy(0,1000)", "");
		element.getSolutionServices().click();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getSolutionServicesElement().isDisplayed());
		
	}
	
	public void EducationCheck()
	{
		ServicesPage element=new ServicesPage(driver);
		 JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("window.scrollBy(0,1000)", "");
		element.getEducationServices().click();
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getEducationElement().isDisplayed());
		
	}

}
