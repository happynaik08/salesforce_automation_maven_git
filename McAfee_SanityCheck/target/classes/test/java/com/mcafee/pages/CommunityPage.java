/**
 * 
 */
package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;

/**
 * @author veenashr
 *
 */
public class CommunityPage {
	
	private WebDriver driver;
	
	PropertiesReader config=new PropertiesReader();
	
	
	public CommunityPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public WebElement getCommunity()
	{
		return driver.findElement(By.xpath(config.getCommunity()));
	}
	
	public WebElement getCommunityElement()
	{
		return driver.findElement(By.xpath(config.getCommunityElement()));
	}
	
	public WebElement getBlogs()
	{
		return driver.findElement(By.xpath(config.getBlogs()));
	}
	
	public WebElement getBlogsElement()
	{
		return driver.findElement(By.xpath(config.getBlogsElement()));
	}
	
	public WebElement getEvents()
	{
		return driver.findElement(By.xpath(config.getEvents()));
	}
	
	public WebElement getEventsElement()
	{
		return driver.findElement(By.xpath(config.getEventsElement()));
	}
	
	public WebElement getWebinars()
	{
		return driver.findElement(By.xpath(config.getWebinars()));
	}
	
	public WebElement getWebinarsElement()
	{
		return driver.findElement(By.xpath(config.getWebinarsElement()));
	}
	
	
	
}
