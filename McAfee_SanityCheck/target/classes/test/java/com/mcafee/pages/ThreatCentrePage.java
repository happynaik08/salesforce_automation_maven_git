/**
 * 
 */
package com.mcafee.pages;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;

/**
 * @author veenashr
 *
 */
public class ThreatCentrePage {
	
	private WebDriver driver;
	PropertiesReader config=new PropertiesReader();


	public  ThreatCentrePage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	public WebElement getThreatCentre()
	{
		return driver.findElement(By.xpath(config.getThreatCentre()));
	}
	
	public WebElement getThreatLibrarySearch()
	{
		return driver.findElement(By.xpath(config.getThreatlibrarySearchtext()));
	}
	
	public WebElement getThreatSearchResult()
	{
		return driver.findElement(By.xpath(config.getThreatSearchResult()));
	}
	
	public WebElement getDownloadCurrentDAT()
	{
		return driver.findElement(By.xpath(config.getDownloadcurrentDAT()));
	}
	
	public WebElement getMcafeeLabs()
	{
		return driver.findElement(By.xpath(config.getMcAfeeLabs()));
	}
	
	public WebElement getMcafeeLabsReadReport()
	{
		return driver.findElement(By.xpath(config.getMcAfeeLabsReadReport()));
	}
	
	public WebElement getDownloadElement()
	{
		return driver.findElement(By.xpath(config.getDownloadElement()));
	}
	
	public WebElement getAgreeBtn()
	{
		return driver.findElement(By.xpath(config.getAgreeBtnThreat()));
	}
	
	//navigation
	
	public WebElement getLastPage()
	{
		return driver.findElement(By.xpath(config.getLastPageThreat()));
	}
	
	public WebElement getNextPage()
	{
		return driver.findElement(By.xpath(config.getNextPageThreat()));
	}
	
	public WebElement getFirstPage()
	{
		return driver.findElement(By.xpath(config.getFirstPageThreat()));
	}
	
	public WebElement getPrevPage()
	{
		return driver.findElement(By.xpath(config.getPrevPageThreat()));
	}
	
	public WebElement getVerificationElement()
	{
		return driver.findElement(By.xpath(config.getVerifyElementThreat()));
	}
	
	
}







