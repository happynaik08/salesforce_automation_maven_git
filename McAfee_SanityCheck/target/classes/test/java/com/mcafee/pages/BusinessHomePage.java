package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;

public class BusinessHomePage 
{
	private WebDriver driver;
	PropertiesReader config=new PropertiesReader();
	
	public BusinessHomePage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	public WebElement getBusiness()
	{
		return driver.findElement(By.xpath(config.getBusiness()));
	}
	
	
	public WebElement getBusinessElement()
	{
		return driver.findElement(By.xpath(config.getBusinessElement()));
	}
	
	
	public WebElement getMenu()
	{
		return driver.findElement(By.xpath(config.getMenu()));
	}
	
	public WebElement getPurchase()
	{
		return driver.findElement(By.xpath(config.getPurchase()));
		
	}
	
	public WebElement getThreatCentre()
	{
		return driver.findElement(By.xpath(config.getThreatCentre()));
		
	}
	
	public WebElement getProducts()
	{
		return driver.findElement(By.xpath(config.getProducts()));
		
	}
	
	public WebElement getSolutions()
	{
		return driver.findElement(By.xpath(config.getSolutions()));
		
	}
	
	public WebElement getServices()
	{
		return driver.findElement(By.xpath(config.getServices()));
		
	}
	
	public WebElement getSupport()
	{
		return driver.findElement(By.xpath(config.getSupport()));
		
	}
	
	public WebElement getPartners()
	{
		return driver.findElement(By.xpath(config.getPartners()));
		
	}
	
	public WebElement getCommunity()
	{
		return driver.findElement(By.xpath(config.getCommunity()));
		
	}
	
	public WebElement getWebinars()
	{
		return driver.findElement(By.xpath(config.getWebinars()));
		
	}
	
	public WebElement getEvents()
	{
		return driver.findElement(By.xpath(config.getEvents()));
		
	}
	
	public WebElement getBlogs()
	{
		return driver.findElement(By.xpath(config.getBlogs()));
		
	}
	
	
	

}
