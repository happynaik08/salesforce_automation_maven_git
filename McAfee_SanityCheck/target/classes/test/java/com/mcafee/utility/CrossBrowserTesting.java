package com.mcafee.utility;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class CrossBrowserTesting {
	
	WebDriver driver;
	public CrossBrowserTesting(WebDriver driver)
	{
		this.driver=driver;
	}

	public void SelectBrowser(String browser, String url) throws Exception
	
	{
		
		
		if(browser.equalsIgnoreCase("firefox")){
			//create firefox instance
				System.setProperty("webdriver.firefox.marionette", ".\\drivers\\geckodriver.exe");
				 final FirefoxProfile firefoxProfile = new FirefoxProfile();
				    firefoxProfile.setPreference("xpinstall.signatures.required", false);
				driver = new FirefoxDriver(firefoxProfile);
				driver.get(url);
			}
			//Check if parameter passed as 'chrome'
			else if(browser.equalsIgnoreCase("chrome")){
				//set path to chromedriver.exe
				System.setProperty("webdriver.chrome.driver",".\\drivers\\chromedriver.exe");
				//create chrome instance
				driver = new ChromeDriver();
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--start-maximized");
				driver = new ChromeDriver( options );
				driver.get(url);
			}
			//Check if parameter passed as 'Edge'
					else if(browser.equalsIgnoreCase("ie")){
						//set path to Edge.exe
						System.setProperty("webdriver.ie.driver","C:\\Users\\veenashr\\workspace\\McAfee_Automation\\drivers\\IEDriverServer.exe");
						//create Edge instance
						driver = new InternetExplorerDriver();
						driver.get(url);
					}
			else{
				//If no browser passed throw exception
				throw new Exception("Browser is not correct");
			}
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}
	

