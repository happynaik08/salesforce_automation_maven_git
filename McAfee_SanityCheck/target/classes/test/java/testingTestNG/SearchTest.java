/**
 * 
 */
package testingTestNG;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


import com.mcafee.pages.Constant_variables;
import com.mcafee.pages.SearchPage;
import com.mcafee.sampletest.EnterprisePageCheck;
import com.mcafee.sampletest.SearchPageCheck;
import com.mcafee.utility.CopyandDeleteFiles;
import com.mcafee.utility.ErrorScreenshot;
import com.mcafee.utility.SendEmail;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



/**
 * @author veenashr
 *
 */

public class SearchTest {
	
	private WebDriver driver;
	ExtentReports report_SearchTest;
	ExtentTest test_SearchTest;
	
	@BeforeClass
	 public void M1(){
	  report_SearchTest = ExtentManager.Instance();
	  
	 }
	@BeforeMethod
	@Parameters({"suite","envname"})
	public void beforesuite(String url_param, String env_param, Method method) throws Exception{
		
		Constant_variables.env=env_param;

		System.out.println("SearchTest started");
		
		System.setProperty("webdriver.chrome.driver",Constant_variables.CHROMEDRIVER_PATH);
		//to maximize window
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(url_param);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("window.scrollBy(0,250)", "");
		EnterprisePageCheck pageload1=new EnterprisePageCheck(driver);
		pageload1.ClickonEnterprise();
		pageload1.isEnterprisePageLoaded();
		
		test_SearchTest=report_SearchTest.startTest("SearchPageCheck - " + method.getName());
		test_SearchTest.assignCategory("Sanity");
	
		}
	
	@Test(priority=9,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void SearchResult1() throws Exception
	{
		
		SearchPageCheck element=new SearchPageCheck(driver);
		element.SearchTest1();
		element.VerfifySearchTest1();
		driver.navigate().back();
		element.Navigationtest();
	}
	
	@Test(priority=9,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void SearchResult2() throws Exception
	{
		SearchPageCheck element=new SearchPageCheck(driver);
		element.SearchTest2();
		element.VerifySearchTest2();
		driver.navigate().back();
		element.Navigationtest();
	}
	

	@AfterMethod
	  public void teardown(ITestResult result) throws Exception 
	  {
		  if(result.getStatus()==ITestResult.FAILURE)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Failed");
			  test_SearchTest.log(LogStatus.FAIL, result.getMethod().getMethodName() +" Test Method Failed");
			  ErrorScreenshot.captureScreenshot(driver, result.getName());
			  System.out.println("Screenshot captured");
		
		}
		  else if (result.getStatus() == ITestResult.SUCCESS)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Passed");
			  test_SearchTest.log(LogStatus.PASS, result.getMethod().getMethodName() +" Test Method Passed");			  
		  }
		  else if (result.getStatus() == ITestResult.SKIP)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Skipped");
			  test_SearchTest.log(LogStatus.SKIP, result.getMethod().getMethodName() +" Test Method Skipped");
		  }
	   driver.close();
	   report_SearchTest.endTest(test_SearchTest);
	   report_SearchTest.flush();
	   
	  }
	
	/*@AfterSuite
	public void aftersuiteHomePage() throws Exception
	{	
	   	CopyandDeleteFiles.copyReport();
		SendEmail.zip();
		SendEmail.sendemail();
		CopyandDeleteFiles.DeleteReport();
		Thread.sleep(1000);
		Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
		driver.quit();
		
	}*/
	
	

}
