/**
 * 
 */
package com.mcafee.sampletest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.mcafee.pages.ProductsPage;

/**
 * @author veenashr
 *
 */
public class ProductsPageCheck {
	
private WebDriver driver;


	
	public ProductsPageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	public void ClickonProducts()
	{
		ProductsPage element=new ProductsPage(driver);
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement products;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		products= wait.until(ExpectedConditions.visibilityOf(element.getProducts()));
		products.click();
		
		driver.switchTo().window(winHandleBefore);
		
	}
	
	public void VerifyProductsPage()
	
	{
		ProductsPage element=new ProductsPage(driver);
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		if(element.getProductsElement()!=null)
		{
			System.out.println("Products Page loaded successfully");
		}
		else 
			System.out.println("Products Page not loaded ");
		
	}
	
		
	public void ClickonAllProducts()
	{	
		
		ProductsPage element=new ProductsPage(driver);
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("window.scrollBy(0,2000)", "");
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement allproducts;
		allproducts= wait.until(ExpectedConditions.visibilityOf(element.getAllProducts()));
		allproducts.click();
		
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getAllProductsElement().isDisplayed());
		
	}
	
	public void EndpointCheck()
	{	
		ProductsPage element=new ProductsPage(driver);
		
		
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement endpoint;
		endpoint= wait.until(ExpectedConditions.visibilityOf(element.getEndpointprotectionSuite()));
		endpoint.click();
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getEndpointElement().isDisplayed());
		driver.navigate().back();
		
	}
	
	public void DatacenterCheck()
	{	
		ProductsPage element=new ProductsPage(driver);
		
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement datacenter;
		datacenter= wait.until(ExpectedConditions.visibilityOf(element.getDatacenterSecurity()));
		datacenter.click();
		
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getDatacenterSecurityElement().isDisplayed());
		driver.navigate().back();
		
	/*	JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("javascript: setTimeout(\"history.go(-1)\", 2000)");*/
		
	}
	
	public void WebProtectionCheck()
	{	
		ProductsPage element=new ProductsPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement webprotection;
		webprotection= wait.until(ExpectedConditions.visibilityOf(element.getWebProtection()));
		webprotection.click();
		
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getWebProtectionElement().isDisplayed());
	}
		
		
	
	
	
	

}
