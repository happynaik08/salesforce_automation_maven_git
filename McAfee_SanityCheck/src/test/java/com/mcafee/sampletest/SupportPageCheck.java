/**
 * 
 */
package com.mcafee.sampletest;

import java.io.File;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.mcafee.pages.Constant_variables;
import com.mcafee.pages.SupportPage;
import com.mcafee.utility.CommonMethods;


/**
 * @author veenashr
 *
 */
public class SupportPageCheck
{
private WebDriver driver;


	public SupportPageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void SupportCheck()
	{
		SupportPage element=new SupportPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement support;  
		
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		support= wait.until(ExpectedConditions.visibilityOf(element.getSupport()));
		support.click();
		
		
		Assert.assertEquals(true, element.getSupportElement().isDisplayed());
		driver.switchTo().window(winHandleBefore);
				
	}
	
	public void ProductDownloads()
	{
		
		SupportPage element=new SupportPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement prod_download;  
		
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		prod_download= wait.until(ExpectedConditions.visibilityOf(element.getProductDwnld()));
		prod_download.click();
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getProductDwnldBtn().isDisplayed());
		driver.switchTo().window(winHandleBefore);
	}
	
	public void ProductDownloadCheck()
	{	
		ProductDownloads();
		SupportPage element=new SupportPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement prod_downloadbtn; 
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		prod_downloadbtn= wait.until(ExpectedConditions.visibilityOf(element.getProductDwnldBtn()));
		prod_downloadbtn.click();
	
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getProductDwnldElement().isDisplayed());
	}
	
	public void FreetrialCheck() throws Exception
	{
		
		SupportPage element=new SupportPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement freetrial; 
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		freetrial= wait.until(ExpectedConditions.visibilityOf(element.getFreetrialBtn()));
		freetrial.click();
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getProductDropdown().isDisplayed());
		driver.switchTo().window(winHandleBefore);
		
	}
	
	public void FreeTrialDataSecurity()
	{	
		SupportPage element=new SupportPage(driver);
		WebElement mySelectElement = element.getProductDropdown();
		Select dropdown= new Select(mySelectElement);
		dropdown.selectByVisibleText("Data Protection & Encryption");
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getDataprotectionVerify().isDisplayed());
	}
	
	public void FreeTrialEndpoint() throws Exception
	{
		SupportPage element=new SupportPage(driver);
		WebElement mySelectElement = element.getProductDropdown();
		Select dropdown= new Select(mySelectElement);
		dropdown.selectByVisibleText("Endpoint Protection");
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getEndpointSecurityVerify().isDisplayed());
		
	//validate navigation
		
		element.getLastPage().click();
		Thread.sleep(3000);
		Assert.assertEquals(true, element.getTableElement().isDisplayed());
		element.getPrevPage().click();
		Thread.sleep(3000);
		Assert.assertEquals(true, element.getTableElement().isDisplayed());
		element.getTableElement().click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getVerificationElement().isDisplayed());
		
	}
	
	public void FreeTrialWebSecurity()
	{
	SupportPage element=new SupportPage(driver);
	WebElement mySelectElement = element.getProductDropdown();
	Select dropdown= new Select(mySelectElement);
	dropdown.selectByVisibleText("Web Security");
	driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
	Assert.assertEquals(true, element.getWebSecurityVerify().isDisplayed());
		
	}
	
	public void FreeToolsCheck()
	{
		SupportPage element=new SupportPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement freetools; 
		
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		freetools= wait.until(ExpectedConditions.visibilityOf(element.getFreetoolsBtn()));
		freetools.click();
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getFreetoolsElement().isDisplayed());
		driver.switchTo().window(winHandleBefore);
	}
	
	public void CheckFreetools1() throws Exception
	{	
		SupportPage element=new SupportPage(driver);
		element.getGetsUp().click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getDownloadGetsup().isDisplayed());
		element.getDownloadGetsup().click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		element.getDownloadsBtn().click();
		
		CommonMethods element2=new CommonMethods(driver);
		element2.DownloadCheck("http://downloadcenter.mcafee.com/products/mcafee-avert/getsusp/getsusp.exe",Constant_variables.Downloaded_getsup_path );
		File file = new File(Constant_variables.Downloaded_getsup_path);
		if(file.exists())
		{
			System.out.println("File downloaded");
			file.delete();
		}
		else
		{
			System.out.println("Error downloading file");
		}
		driver.navigate().back();
		driver.navigate().back();
	}
	
	public void CheckFreetools2() throws Exception
	{	
		SupportPage element=new SupportPage(driver);
		element.getStinger().click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getDownloadStringer().isDisplayed());
		element.getDownloadStringer().click();
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		element.getDownloadsBtn().click();
		
		CommonMethods element2=new CommonMethods(driver);
		element2.DownloadCheck("http://downloadcenter.mcafee.com/products/mcafee-avert/Stinger/stinger32.exe",Constant_variables.Downloaded_stringer_path);
		File file = new File(Constant_variables.Downloaded_stringer_path);
		if(file.exists())
		{
			System.out.println("File downloaded");
			file.delete();
		}
		else
		{
			System.out.println("Error downloading file");
		}
	}
	
	
	
	public void ClickonSecurityUpdates()
	{	
		SupportPage element=new SupportPage(driver);
		
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement Securityupdates; 
		
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		Securityupdates= wait.until(ExpectedConditions.visibilityOf(element.getSecurityUpdates()));
		Securityupdates.click();
		
		element.getAgreeButton().click();
		
	
		if(element.getSecurityUpdatesElement()!=null)
		{
			System.out.println("Security Page load Successful");
		
		if(element.getDATPackageDownload()!=null)
		{
			System.out.println("Download results loaded fine");
		}
		
		}
		else
		{
			System.out.println("Security Page failed to load");
		}
		driver.switchTo().window(winHandleBefore);
		
	}
	
	
}

