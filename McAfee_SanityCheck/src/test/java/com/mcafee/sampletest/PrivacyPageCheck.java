/**
 * 
 */
package com.mcafee.sampletest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.mcafee.pages.PrivacyPage;




/**
 * @author veenashr
 *
 */
public class PrivacyPageCheck {
	
	private WebDriver driver;
	
	
	public PrivacyPageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	
	public void ClickonPrivacy()
	{
		PrivacyPage element=new PrivacyPage(driver);
		
		element.getPrivacy().click();
	
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		Assert.assertEquals(true, element.getTab1Element().isEnabled());
		System.out.println("Privacy page sucessfully loaded");
		
	}
	
	public void VerifyTab1()
	{
		PrivacyPage element=new PrivacyPage(driver);
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement tab1,tab1e;
		tab1= wait.until(ExpectedConditions.visibilityOf(element.getTab1()));
		tab1.click();
		tab1e=wait.until(ExpectedConditions.visibilityOf(element.getTab1Element()));
		Assert.assertEquals( tab1e.isDisplayed(),true);
		
	}
		
	public void VerifyTab2()
	{
		PrivacyPage element=new PrivacyPage(driver);
		WebDriverWait wait = new WebDriverWait(driver,100);
		WebElement tab2,tab2e;
		tab2= wait.until(ExpectedConditions.visibilityOf(element.getTab2()));
		tab2.click();
		tab2e=wait.until(ExpectedConditions.visibilityOf(element.getTab2Element()));
		Assert.assertEquals( tab2e.isDisplayed(),true);
	}
		
		
	public void VerifyTab3()
	{
		PrivacyPage element=new PrivacyPage(driver);
		WebDriverWait wait = new WebDriverWait(driver,100);
		WebElement tab3,tab3e;
		tab3= wait.until(ExpectedConditions.visibilityOf(element.getTab3()));
		tab3.click();
		tab3e=wait.until(ExpectedConditions.visibilityOf(element.getTab3Element()));
		Assert.assertEquals( tab3e.isDisplayed(),true);
	}
		
		
	
	
	
	

}
