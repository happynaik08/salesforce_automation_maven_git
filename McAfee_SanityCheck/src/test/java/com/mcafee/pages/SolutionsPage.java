/**
 * 
 */
package com.mcafee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.mcafee.utility.PropertiesReader;

/**
 * @author veenashr
 *
 */
public class SolutionsPage {
	
	private WebDriver driver;
	PropertiesReader config=new PropertiesReader();
	

	
	public SolutionsPage(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public WebElement getSolutions()
	{
		return driver.findElement(By.xpath(config.getSolutions()));
	}
	
	public WebElement getSolutionsElement()
	{
		return driver.findElement(By.xpath(config.getSolutionsElement()));
	}
	
	public WebElement getDataCenter()
	{
		return driver.findElement(By.linkText(config.getDataCenterText()));
	}
	
	public WebElement getDatacenterElement()
	{
		return driver.findElement(By.xpath(config.getDataCenterElement()));
	}
	
	public WebElement getTechnologyTab()
	{
		return driver.findElement(By.xpath(config.getTechnology()));
	}
	
	public WebElement getEmbeddedSecurity()
	{
		return driver.findElement(By.xpath(config.getEmbeddedSecurity()));
	}
	
	public WebElement getEmbeddedSecurityElement()
	{
		return driver.findElement(By.xpath(config.getEmbeddedSecurityElement()));
	}
	
	/*public WebElement getCriticalInfra()
	{
		return driver.findElement(criticalinfra_xpath);
	}
	
	public WebElement getCriticalInfraElement()
	{
		return driver.findElement(criticalinfraelement_xpath);
	}
	*/
	
	
	
}

