package testingTestNG;

import java.io.File;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author magesh
 *
 */
public class BaseExample {
	    protected ExtentReports extent;
	    protected ExtentTest test;
	    
	    final String filePath = "./test_output/Automatuion_Report.html";
	    
	    @BeforeSuite
	    public void beforeSuite() {
	    	System.out.println("Base Example Before Suite executed");
	    	extent = new ExtentReports(filePath, true);
	    	extent.addSystemInfo("Environment", "QA");
            extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent_config.xml"));
	    }
	    
	    @AfterMethod
	    protected void afterMethod(ITestResult result) {
	        if (result.getStatus() == ITestResult.FAILURE) {
	            test.log(LogStatus.FAIL, result.getThrowable());
	        } else if (result.getStatus() == ITestResult.SKIP) {
	            test.log(LogStatus.SKIP, "Test skipped " + result.getThrowable());
	        } else {
	            test.log(LogStatus.PASS, "Test passed");
	        }
	        
	        System.out.println("Base Example After Method executed");
	        extent.endTest(test);
	    }
	    
	    @AfterSuite
	    protected void afterSuite() {
	    	
	    	extent.flush();
	        System.out.println("Base Example After Suite executed");
	    }
	}
