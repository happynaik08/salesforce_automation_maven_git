/**
 * 
 */
package testingTestNG;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.mcafee.pages.BusinessHomePage;
import com.mcafee.pages.Constant_variables;
import com.mcafee.sampletest.EnterprisePageCheck;

import com.mcafee.sampletest.ThreatCentrePageCheck;

import com.mcafee.utility.CopyandDeleteFiles;
import com.mcafee.utility.ErrorScreenshot;
import com.mcafee.utility.SendEmail;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


/**
 * @author veenashr
 *
 */

public class ThreatCentrePageTest {
	
	private WebDriver driver;
	ExtentReports report_ThreatCentrePageTest;
	ExtentTest test_ThreatCentrePageTest;
	
	@BeforeClass
	 public void M1(){
	  report_ThreatCentrePageTest = ExtentManager.Instance();
	  
	 }
	
	@BeforeMethod
	@Parameters({"suite","envname"})
	public void beforesuite(String url_param, String env_param) throws Exception{
		
		Constant_variables.env=env_param;
		
		System.out.println("ThreatCentrePageTest started");
		
		System.setProperty("webdriver.chrome.driver",Constant_variables.CHROMEDRIVER_PATH);
		//to maximize window
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		driver = new ChromeDriver( options );
		driver.get(url_param);
		
		
		driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		EnterprisePageCheck pageload1=new EnterprisePageCheck(driver);
		
		 JavascriptExecutor jse = (JavascriptExecutor)driver;
		 jse.executeScript("window.scrollBy(0,250)", "");
		    
		pageload1.ClickonEnterprise();
		pageload1.isEnterprisePageLoaded();
		BusinessHomePage element=new  BusinessHomePage(driver);
		element.getMenu().click();
		test_ThreatCentrePageTest=report_ThreatCentrePageTest.startTest("ThreatCentrePagetest");
		test_ThreatCentrePageTest.assignCategory("Sanity");
		
		}
	
	
	
	@Test(priority=1,retryAnalyzer=com.mcafee.utility.Re_executeFailedTest.class)
	public void ThreatCentrePagetest() throws Exception
	{	
		ThreatCentrePageCheck element=new ThreatCentrePageCheck(driver);
		element.ClickonThreatCentre();
		element.EnterSearchText();
		element.Navigationtest();
		element.VerifySearchResults();
	}
	
	
	
	@AfterMethod
	  public void teardown(ITestResult result) throws Exception 
	  {
		  if(result.getStatus()==ITestResult.FAILURE)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Failed");
			  test_ThreatCentrePageTest.log(LogStatus.FAIL, result.getMethod().getMethodName() +" Test Method Failed");
			  ErrorScreenshot.captureScreenshot(driver, result.getName());
			  System.out.println("Screenshot captured");
		
		}
		  else if (result.getStatus() == ITestResult.SUCCESS)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Passed");
			  test_ThreatCentrePageTest.log(LogStatus.PASS, result.getMethod().getMethodName() +" Test Method Passed");			  
		  }
		  else if (result.getStatus() == ITestResult.SKIP)
		  {
			  System.out.println( result.getMethod().getMethodName() +" Test Method Skipped");
			  test_ThreatCentrePageTest.log(LogStatus.SKIP, result.getMethod().getMethodName() +" Test Method Skipped");
		  }
	   driver.close();
	   report_ThreatCentrePageTest.endTest(test_ThreatCentrePageTest);
	   report_ThreatCentrePageTest.flush();
	  }
	
	/*@AfterClass
	public void tear()
	{
		report_ThreatCentrePageTest.endTest(test_ThreatCentrePageTest);
		report_ThreatCentrePageTest.flush();
	}*/
	/*@AfterSuite
	public void aftersuiteHomePage() throws Exception
	{	
	   	CopyandDeleteFiles.copyReport();
		SendEmail.zip();
		SendEmail.sendemail();
		CopyandDeleteFiles.DeleteReport();
		Thread.sleep(1000);
		Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
		driver.quit();
		
	}*/


	}
	
	
	

	
	
	
	


