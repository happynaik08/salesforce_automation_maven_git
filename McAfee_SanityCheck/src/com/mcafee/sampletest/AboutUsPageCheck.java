/**
 * 
 */
package com.mcafee.sampletest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mcafee.pages.AboutUsPage;




/**
 * @author veenashr
 *
 */
public class AboutUsPageCheck 
{
private WebDriver driver;
	
	
	public AboutUsPageCheck(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void ClickonAboutUs()
	{
		AboutUsPage element1=new AboutUsPage(driver);
		WebDriverWait wait = new WebDriverWait(driver,60);
		WebElement about_us;
		String winHandleBefore = driver.getWindowHandle();
		for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}
		driver.manage().timeouts().implicitlyWait(1000, TimeUnit.SECONDS);
		about_us= wait.until(ExpectedConditions.visibilityOf(element1.getAboutUs()));
		about_us.click();
		
		driver.switchTo().window(winHandleBefore);
		
	}
	
	
	public void isAboutUsPageLoaded() throws Exception
	{  
		AboutUsPage element2=new AboutUsPage(driver);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		
		if(element2.getAboutuselement()!=null)
		{   
			System.out.println("AboutUsPage load succesful ");
			
		}
		
		else
		{
			System.out.println("AboutUsPage not found ");
		}
	}
	
}
