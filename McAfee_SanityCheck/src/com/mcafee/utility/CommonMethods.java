/**
 * 
 */
package com.mcafee.utility;


import java.io.FileOutputStream;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;



import org.openqa.selenium.WebDriver;

import com.mcafee.pages.Constant_variables;



/**
 * @author veenashr
 *
 */
public class CommonMethods {
	
private WebDriver driver;


	
	public CommonMethods(WebDriver driver)
	{
		this.driver=driver;
	}
	
	public void DownloadCheck(String urltocheck,String path) throws Exception
	{
		 URL url = new URL(urltocheck);
		  HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		  int responseCode = urlConnection.getResponseCode();
		  
	        // always check HTTP response code first
	        if (responseCode == HttpURLConnection.HTTP_OK)
	        {
	        	System.out.println("URL avaialable");
	       
	       InputStream inputStream = urlConnection.getInputStream();
	        FileOutputStream outputStream = new FileOutputStream(path);
	        outputStream.close();
           inputStream.close();
     //      System.out.println("File downloaded");
          } 
	        else {
           System.out.println("No file to download. Server replied HTTP code: " + responseCode);
       }
	        
       urlConnection.disconnect();
	}

	
	

}

