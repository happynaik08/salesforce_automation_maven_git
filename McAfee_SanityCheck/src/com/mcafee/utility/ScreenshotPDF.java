/**
 * 
 */
package com.mcafee.utility;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.testng.ITestResult;

/**
 * @author veenashr
 *
 */
public class ScreenshotPDF  {
	
	public static void GenerateSSPdf() throws DocumentException, IOException{
		ITestResult result = null;
		@SuppressWarnings("null")
		String screenshotname=result.getName();
		String file = System.getProperty("user.dir")+"\\"+screenshotname+".png";
		
		
	Document document = new Document();
    PdfWriter.getInstance(document, new FileOutputStream("ErrorScreenshots.pdf"));
    document.open();
    int m=new File(System.getProperty("user.dir")+"\\Screenshots").listFiles().length;
    for(int i=0;i<m;i++)
    {
    Image img = Image.getInstance(file);
    document.add(new Paragraph(screenshotname));
    document.add(img);
    
    }
    document.close();
    System.out.println("Done");
}
}
