/**
 * 
 */
package com.mcafee.pages;

import java.io.File;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import testingTestNG.HomePageTest;

/**
 * @author veenashr
 *
 */
public class Constant_variables {
	
	public static String env;
	
	public static final String FOLDER_PATH=System.getProperty("user.dir")+"\\";
	
	
	public final String  SCREENSHOT_PATH_PROD=FOLDER_PATH+"Screenshots-prod";
	public static final  String SCREENSHOT_PATH_DR=FOLDER_PATH+"Screenshots-dr";
	public static final  String REPORT_PATH_PROD=FOLDER_PATH+"Mcafee-prod-report"+".pdf";
	public static final  String REPORT_PATH_DR=FOLDER_PATH+"Mcafee-dr-report"+".pdf";
	
	public static final String CHROMEDRIVER_PATH=System.getProperty("user.dir")+"\\drivers\\chromedriver.exe";
	public static final String Downloaded_dat_path= ".\\Downloads\\avvepo8509dat.zip";
	public static final String Downloaded_getsup_path= ".\\Downloads\\getsusp.exe";
	public static final String Downloaded_stringer_path= ".\\Downloads\\stinger32.exe";
	


	
	
}
