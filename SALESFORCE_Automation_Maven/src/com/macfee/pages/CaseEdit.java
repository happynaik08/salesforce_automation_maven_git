package com.macfee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import com.mcafee.framework.AutomationHelper;

/**
*
* @author Puneeth & Balamurugan
*
*/

public class CaseEdit 
{
	private WebDriver driver;
	private By sfGroupDDField=By.id("00N3600000QVKtF");
	private By sfSourceDDField=By.id("00N3600000QVKtj");
	private By sfTierDDField=By.id("00N3600000QVKtn");
	private By sfTypeDDField=By.id("00N3600000QVKt2");
	private By sfStatusDDField=By.id("cas7");
	//private By sfSeverityDDField=By.id("00N3600000QVKti");
	private By sfAssitanceTypeDDField=By.id("00N3600000QVKsu");
	private By sfCaseOriginDDField=By.id("cas11");
	private By sfInitialQueueDDField=By.id("00NV000000136qN");
	private By sfVendoreNameDDField=By.id("00N3600000QVKto");
	private By sfVendorSiteDDField=By.id("00N3600000QVKtp");
	private By sfPartnerSupportDDField=By.id("00N3600000QVKtV");
	private By sfProductSuiteDDField=By.id("00N3600000QVKtd");
	private By sfReleaseNameDDField=By.id("00N3600000QVKtg");
	private By sfPointProductDDField=By.id("00N3600000QVKtZ");
	private By sfPointProductVerDDField=By.id("00N3600000QVKtY");
	private By sfCategoryDDField=By.id("00N3600000QVKt3");
	private By sfSubcategoryDDField=By.id("00N3600000QVKtl");
	private By sfResellerDDField=By.id("00N3600000QVKth");
	private By sfKBDocIDTxtField=By.id("00N3600000QVKtI");
	private By sfContactTxtField=By.id("cas3");//00N7A000000mcrk
	private By sfSubjectTxtField=By.id("cas14");
	private By sfDescriptionTxtField=By.id("cas15");
	private By sfSaveBtnField=By.xpath(".//input[@title='Save']");
	private By sfCreatedByField=By.id("CreatedBy_ileinner");
	private By sfCancelBtnField=By.xpath("//input[@title = 'Cancel']");
	private By sfPrimaryEmailAddressField=By.xpath("//*[@id='00N3600000QVKtb_ileinner']/a");

	CaseEdit(WebDriver driver) 
	{
		this.driver = driver;
	}
	
	public void verifyIfCaseEditPage() 
	{
		Assert.assertEquals(getSaveBtnField().isDisplayed(), true, "In New Case page");
	}
	
	public boolean isCaseEditPageLaunched()
	{
		return AutomationHelper.findElement(driver,By.id("cas14"), 20).isDisplayed();
	}
	
	public WebElement getGroupDDField()
	{
		return AutomationHelper.findElement(driver, sfGroupDDField, 10);
	}
	
	public WebElement getSourceDDField()
	{
		return AutomationHelper.findElement(driver, sfSourceDDField, 10);
	}
	
	public WebElement getTierDDField()
	{
		return AutomationHelper.findElement(driver, sfTierDDField, 10);
	}
	
	public WebElement getTypeDDField()
	{
		return AutomationHelper.findElement(driver, sfTypeDDField, 10);
	}
	
	public WebElement getStatusDDField()
	{
		return AutomationHelper.findElement(driver, sfStatusDDField, 10);
	}
	
	/*
	 * public WebElement getSeverityDDField()
	{
		return AutomationHelper.findElement(driver, sfSeverityDDField, 10);
	}
	*/
	
	public WebElement getAssitanceTypeDDField()
	{
		return AutomationHelper.findElement(driver, sfAssitanceTypeDDField, 10);
	}
	
	public WebElement getCaseOriginDDField()
	{
		return AutomationHelper.findElement(driver, sfCaseOriginDDField, 10);
	}
	
	public WebElement getVendoreNameDDField()
	{
		return AutomationHelper.findElement(driver, sfVendoreNameDDField, 10);
	}
	
	public WebElement getInitialQueueDDField()
	{
		return AutomationHelper.findElement(driver, sfInitialQueueDDField, 10);
	}
	
	public WebElement getVendorSiteDDField()
	{
		return AutomationHelper.findElement(driver, sfVendorSiteDDField, 10);
	}
	
	public WebElement getPartnerSupportDDField()
	{
		return AutomationHelper.findElement(driver, sfPartnerSupportDDField, 10);
	}
	
	public WebElement getProductSuiteDDField()
	{
		return AutomationHelper.findElement(driver, sfProductSuiteDDField, 10);
	}

	public WebElement getReleaseNameDDField()
	{
		return AutomationHelper.findElement(driver, sfReleaseNameDDField, 10);
	}
	
	public WebElement getPointProductDDField()
	{
		return AutomationHelper.findElement(driver, sfPointProductDDField, 10);
	}
	
	public WebElement getPointProductVerDDField()
	{
		return AutomationHelper.findElement(driver, sfPointProductVerDDField, 10);
	}
	
	public WebElement getCategoryDDField()
	{
		return AutomationHelper.findElement(driver, sfCategoryDDField, 10);
	}
	
	public WebElement getSubcategoryDDField()
	{
		return AutomationHelper.findElement(driver, sfSubcategoryDDField, 10);
	}
	
	public WebElement getResellerDDField()
	{
		return AutomationHelper.findElement(driver, sfResellerDDField, 10);
	}
	
	public WebElement getKBDocIDTxtField()
	{
		return AutomationHelper.findElement(driver, sfKBDocIDTxtField, 10);
	}
	
	public WebElement getContactTxtField()
	{
		return AutomationHelper.findElement(driver, sfContactTxtField, 10);
	}
	
	public WebElement getSubjectTxtField()
	{
		return AutomationHelper.findElement(driver, sfSubjectTxtField, 10);
	}
	
	public WebElement getDescriptionTxtField()
	{
		return AutomationHelper.findElement(driver, sfDescriptionTxtField, 10);
	}
	
	public WebElement getSaveBtnField()
	{
		return AutomationHelper.findElement(driver, sfSaveBtnField, 10);
	}
	
	public WebElement getCreatedByField() 
	{
		return AutomationHelper.findElement(driver, sfCreatedByField, 10);
	}
	
	public WebElement getCancelBtnField() 
	{
		return AutomationHelper.findElement(driver, sfCancelBtnField, 10);
	}
	
	public WebElement getPrimaryEmailAddressField() 
	{
		return AutomationHelper.findElement(driver, sfPrimaryEmailAddressField, 10);
	}
	
	public void selectAgentGroup(String agentgroup)
	{
		Select dropdown= new Select(getGroupDDField()); 
		dropdown.selectByVisibleText(agentgroup); 
		System.out.println("Group is " + agentgroup);
	}
		
	public void selectSource(String source)
	{
		Select dropdown= new Select(getSourceDDField()); 
		dropdown.selectByVisibleText(source); 
		System.out.println("Source is " + source);
	}
		
	public void selectTier(String tier)
	{
		Select dropdown= new Select(getTierDDField()); 
		dropdown.selectByVisibleText(tier); 
		System.out.println("Tier is " + tier);
	}
		
	public void selectType(String type)
	{
		Select dropdown= new Select(getTypeDDField()); 
		dropdown.selectByVisibleText(type); 
		System.out.println("Type is " + type);
	}
		
	public void selectStatus(String status)
	{
		Select dropdown= new Select(getStatusDDField()); 
		dropdown.selectByVisibleText(status); 
		System.out.println("Status is " + status);
	}
	
	/*
	public void selectSeverity(String severity)
	{
		Select dropdown= new Select(getSeverityDDField()); 
		dropdown.selectByVisibleText(severity); 
		System.out.println("Severity is " + severity);
	}
	*/	
	
	public void selectAssitanceType(String assitancetype)
	{
		Select dropdown= new Select(getAssitanceTypeDDField()); 
		dropdown.selectByVisibleText(assitancetype); 
		System.out.println("Assitance type is " + assitancetype);
	}
		
	public void selectCaseOrigin(String caseorigin)
	{
		Select dropdown= new Select(getCaseOriginDDField()); 
		dropdown.selectByVisibleText(caseorigin); 
		System.out.println("Case origin is " + caseorigin);
	}
	
	public void selectInitialQueue(String initialqueue)
	{
		Select dropdown= new Select(getInitialQueueDDField()); 
		dropdown.selectByVisibleText(initialqueue); 
		System.out.println("Initial Queue is " + initialqueue);
	}
		
	public void selectVendorName(String vendorName)
	{
		Select dropdown= new Select(getVendoreNameDDField()); 
		dropdown.selectByVisibleText(vendorName); 
		System.out.println("Vendor name is " + vendorName);
	}
		
	public void selectVendorSite(String vendorSite)
	{
		Select dropdown= new Select(getVendorSiteDDField()); 
		dropdown.selectByVisibleText(vendorSite); 
		System.out.println("Vendor site is " + vendorSite);
	}
		
	public void selectPartnerSupport(String partnerSupport)
	{
		Select dropdown= new Select(getPartnerSupportDDField()); 
		dropdown.selectByVisibleText(partnerSupport); 
		System.out.println("Partner support is " + partnerSupport);
	}
		
	public void selectProductSuite(String productSuite)
	{
		Select dropdown= new Select(getProductSuiteDDField()); 
		dropdown.selectByVisibleText(productSuite); 
		System.out.println("Product suite is " + productSuite);
	}
	
	public void selectReleaseName(String releaseName)
	{
		Select dropdown= new Select(getReleaseNameDDField()); 
		dropdown.selectByVisibleText(releaseName); 
		System.out.println("Release name is " + releaseName);
	}
	
	public void selectPointProduct(String pointProduct)
	{
		Select dropdown= new Select(getPointProductDDField()); 
		dropdown.selectByVisibleText(pointProduct); 
		System.out.println("Point product is " + pointProduct);
	}
	
	public void selectPointProductVer(String pointProductVer)
	{
		Select dropdown= new Select(getPointProductDDField()); 
		dropdown.selectByVisibleText(pointProductVer); 
		System.out.println("Point product Version is " + pointProductVer);
	}
	
	public void selectCategory(String category)
	{
		Select dropdown= new Select(getCategoryDDField()); 
		dropdown.selectByVisibleText(category); 
		System.out.println("Category is " + category);
	}
		
	public void selectSubcategory(String subcategory)
	{
		Select dropdown= new Select(getSubcategoryDDField()); 
		dropdown.selectByVisibleText(subcategory); 
		System.out.println("Point product is " + subcategory);
	}
	
	public void selectReseller(String reseller)
	{
		Select dropdown= new Select(getResellerDDField()); 
		dropdown.selectByVisibleText(reseller); 
		System.out.println("Reseller is " + reseller);
	}
	
	public void putKBDoc(String KBDoc)
	{
		getKBDocIDTxtField().sendKeys(KBDoc);
		System.out.println("KB Doc ID is " + KBDoc);
	}
	
	public void putContact(String contactName)
	{
		getContactTxtField().sendKeys(contactName);
		System.out.println("Consumer Name is " + contactName);
	}
	
	public void putSubject(String subject)
	{
		getSubjectTxtField().sendKeys(subject);
		System.out.println("Subject is " + subject);
	}
	
	public void putDescription(String description)
	{
		getDescriptionTxtField().sendKeys(description);
		System.out.println("Description is " + description);
	}
	
	public String contactName()
	{
		return getContactTxtField().getText();
	}
	
	public String contactPrimaryEmailAddress()
	{
		return getPrimaryEmailAddressField().getText();
	}
	
	public String getCreatedBy() 
	{
		return getCreatedByField().getText();
	}
	
	public Case clickCancel() 
	{
		getCancelBtnField().click();
		return new Case(driver);
	}
	
	public Case onClickSave() 
	{
		getSaveBtnField().click();
		System.out.println("Save button clicked");
		return new Case(driver);
	}
}
