package com.mcafee.samplemaven;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.macfee.pages.Case;
import com.macfee.pages.CaseEdit;
import com.macfee.pages.HomePage;
import com.macfee.pages.LoginPage;
import com.macfee.pages.NewCasePage;
import com.mcafee.framework.BaseClass;
import com.mcafee.framework.ConfigData;
import com.mcafee.framework.DataProviderClass;

/**
*
* @author Balamurugan
*
*/

public class TestCaseCreationFromSF extends BaseClass
{
	private static LoginPage loginPage;
	private static HomePage home;
	private static Case caseTab;
	private static NewCasePage newCase;
	private static CaseEdit caseEdit;
	private static DataProviderClass dataproviders;
	static int i;
	
	@Override
	@BeforeClass
	public void setup() 
	{
		super.setup();
		i=1;
		System.out.println("Setup done SF");
		loginPage=commonMethods.launchSFApplication();
		home=loginPage.doLogIn(ConfigData.SFUSERNAME, ConfigData.SFPASSWORD);
		//Assert.assertTrue(false, "Home page launched successfully");
		Assert.assertTrue(home.isHomePageLaunched(), "SF home page launched successfully");
		System.out.println("Case Creator from SF");
	}
	
	@Test(dataProvider="sfCaseCreator",dataProviderClass=DataProviderClass.class)
	public void test(String...testData) throws Exception
	{
		caseTab=home.onClickCaseTab();
		newCase=caseTab.onClickNewButton();
		newCase.selectCaseRecordType(testData[0]);
		caseEdit=newCase.onClickContinue();
		caseEdit.selectAgentGroup(testData[1]);
		caseEdit.selectSource(testData[2]);
		caseEdit.selectTier(testData[3]);
		caseEdit.selectType(testData[4]);
		caseEdit.selectStatus(testData[5]);
		//caseEdit.selectSeverity(testData[6]);
		caseEdit.selectAssitanceType(testData[6]);
		caseEdit.selectCaseOrigin(testData[7]);
		caseEdit.selectInitialQueue(testData[8]);
		caseEdit.selectVendorName(testData[9]);
		caseEdit.selectVendorSite(testData[10]);
		caseEdit.selectPartnerSupport(testData[11]);
		caseEdit.selectProductSuite(testData[12]);
		caseEdit.selectReleaseName(testData[13]);
		caseEdit.selectPointProduct(testData[14]);
		caseEdit.selectPointProductVer(testData[15]);
		caseEdit.selectCategory(testData[16]);
		caseEdit.selectSubcategory(testData[17]);
		caseEdit.selectReseller(testData[18]);
		caseEdit.putKBDoc(testData[19]);
		caseEdit.putContact(testData[20]);
		caseEdit.putSubject(testData[21]);
		caseEdit.putDescription(testData[22]);
		caseEdit.onClickSave();
		String casenum=caseTab.verifyCaseCreated();
		Assert.assertNotNull(casenum, "Case created in SF Application");
		dataproviders.writeDataToExcel("./resources/TestData.xlsx", "SFCaseCreations", casenum, i ,testData.length);
	} 
	
	@AfterMethod
	public void colno()
    {
		i++;
	}
}