package com.macfee.pages;

import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import com.mcafee.framework.AutomationHelper;

/**
*
* @author Puneeth & Balamurugan
*
*/

public class Case 
{
	private WebDriver driver;
	private String sfExpTitle = "Consumer Contacts: Home ~ Salesforce - Unlimited Edition";
	private By sfCaseStatusField=By.id("cas7_ileinner");
	private By sfCaseOwnerField=By.id("cas1_ileinner");
	private By sfCaseNumberField=By.id("cas2_ileinner");
	private By sfNewCaseBtnField=By.xpath(".//input[@title='New']");
	private By sfCaseEditField=By.xpath("//input[@title = 'Edit']");
	private By sfCaseStringField=By.xpath("//span[@class='efhpCenterValue  ']/span"); ////*[@id="contactHeaderRow"]/div[2]/h2
	private By sfCaseDetailsField=By.xpath("//a[starts-with(@class, 'optionItem efpDetailsView')]");
	private By sfCaseFeedField=By.xpath("//a[starts-with(@class, 'optionItem efpFeedView')]");
	private By sfAcceptOpenField=By.xpath("//input[@title = 'Accept & Open']");
	private By sfEmailExpandField=By.id("collapsed_5007A000003XuVe"); //By.xpath("//div[starts-with(@id ,'collapsed_')]/input");
	private By sfEmailExpandECATField=By.xpath("//div[starts-with(@id ,'switchOptionsId_')]/input");
	private By sfSendEmailField=By.id("emailPublisherSubmitBtn_5007A000003XuVe");
	private By sfMoreEmailOptionsField=By.xpath(".//input[@title='Show more email options']");
	private By sfEmailFromField=By.xpath("//select[starts-with(@id ,'from_')]");
	private By sfEmailToField=By.xpath("//textarea[starts-with(@id ,'to_')]");
	private By sfEmailCCField=By.xpath("//textarea[starts-with(@id ,'cc')]");
	private By sfEmailAddCCField=By.xpath("//a[starts-with(@id ,'addCc_')]");
	private By sfEmailSubjectField=By.xpath("//textarea[starts-with(@id ,'subject_')]");
	private By sfEmailBodyField=By.xpath("//textarea[starts-with(@id ,'subject_')]"); //By.xpath("//textarea[starts-with(@id ,'emailPublisherRte_')]");
	private By sfEmailTemplateField=By.xpath("//span[contains(text(),'Select a Template')]");
	private By sfTemplateFolderField=By.id("FolderSelect");
	private By sfTemplateNameField=By.xpath("//div[starts-with(text(),templateName)]");
	private By sfCaseStatusEmailField=By.id("cas7__Em");
	private By sfRelatedEmailMsgListField=By.xpath("//div[contains(@id, 'RelatedEmailMessageList_body')]/table/tbody/tr[2]/td[5]");
	
	Case(WebDriver driver)
	{
		this.driver = driver;
	}
	
	public void verifyIfCaseTab()
	{
		String sfActTitle = driver.getTitle();
		Assert.assertEquals(sfActTitle, sfExpTitle);
	}
	
	public WebElement getCaseStringField()
	{
		return AutomationHelper.findElement(driver, sfCaseStringField, 10);
	}
	
	public WebElement getCaseEditField()
	{
		return AutomationHelper.findElement(driver, sfCaseEditField, 10);
	}
	
	public WebElement getCaseStatusField()
	{
		return AutomationHelper.findElement(driver, sfCaseStatusField, 10);
	}
	
	public WebElement getCaseOwnerField()
	{
		return AutomationHelper.findElement(driver, sfCaseOwnerField, 10);
	}
	
	public WebElement getCaseNumberField()
	{
		return AutomationHelper.findElement(driver, sfCaseNumberField, 10);
	}
	
	public WebElement getNewCaseBtnField()
	{
		return AutomationHelper.findElement(driver, sfNewCaseBtnField, 10);
	}
	
	public WebElement getCaseDetailsField()
	{
		return AutomationHelper.findElement(driver, sfCaseDetailsField, 10);
	}
	
	public WebElement getCaseFeedField()
	{
		return AutomationHelper.findElement(driver, sfCaseFeedField, 10);
	}
	
	public WebElement getAcceptOpenField()
	{
		return AutomationHelper.findElement(driver, sfAcceptOpenField, 10);
	}
	
	public WebElement getEmailExpandField()
	{
		return AutomationHelper.findElement(driver, sfEmailExpandField, 10);
	}
	
	public WebElement getEmailExpandECATField()
	{
		return AutomationHelper.findElement(driver, sfEmailExpandECATField, 10);
	}
	
	public WebElement getSendEmailField()
	{
		return AutomationHelper.findElement(driver, sfSendEmailField, 10);
	}
	
	public WebElement getMoreEmailOptionsField()
	{
		return AutomationHelper.findElement(driver, sfMoreEmailOptionsField, 10);
	}
	
	public WebElement getEmailFromField()
	{
		return AutomationHelper.findElement(driver, sfEmailFromField, 10);
	}
	
	public WebElement getEmailToField()
	{
		return AutomationHelper.findElement(driver, sfEmailToField, 10);
	}
	
	public WebElement getEmailCCField()
	{
		return AutomationHelper.findElement(driver, sfEmailCCField, 10);
	}
	
	public WebElement getEmailAddCCField()
	{
		return AutomationHelper.findElement(driver, sfEmailAddCCField, 10);
	}
	
	public WebElement getEmailSubjectField()
	{
		return AutomationHelper.findElement(driver, sfEmailSubjectField, 10);
	}
	
	public WebElement getEmailBodyField()
	{
		return AutomationHelper.findElement(driver, sfEmailBodyField, 10);
	}
	
	public WebElement getEmailTemplateField()
	{
		return AutomationHelper.findElement(driver, sfEmailTemplateField, 10);
	}
	
	public WebElement getTemplateFolderField()
	{
		return AutomationHelper.findElement(driver, sfTemplateFolderField, 10);
	}
	
	public WebElement getTemplateNameField()
	{
		return AutomationHelper.findElement(driver, sfTemplateNameField, 10);
	}
	
	public WebElement getCaseStatusEmailField()
	{
		return AutomationHelper.findElement(driver, sfCaseStatusEmailField, 10);
	}
	
	public WebElement getRelatedEmailMsgListField()
	{
		return AutomationHelper.findElement(driver, sfRelatedEmailMsgListField, 10);
	}
	
	public String verifyCaseCreated()
	{	
		String sfCaseString = getCaseStringField().getText();
		//System.out.println("String with case number " + sfCaseString);
		Pattern intsOnly = Pattern.compile("\\d+");
		Matcher makeMatch = intsOnly.matcher(sfCaseString);
		makeMatch.find();
		System.out.println(makeMatch.group() + " Case created in SF Application");
		return makeMatch.group();
	}
	
	public void clickCaseDetails()
	{
		getCaseDetailsField().click();
	}
	
	public void clickCaseFeed()
	{
		getCaseFeedField().click();
	}
	
	public void clickEmailExpand()
	{
		getEmailExpandField().click();
	}
	
	public void clickEmailExpandECAT()
	{
		getEmailExpandECATField().click();
	}
	
	public void clickShowMoreEmailOptions() throws InterruptedException
	{
		getMoreEmailOptionsField().click();
		Thread.sleep(1000);
	}
	
	public String caseStatus() 
	{
		return getCaseStatusField().getText();
	}
	
	public String caseOwner() 
	{
		return getCaseOwnerField().getText();
	}
	
	public String getCaseNumber() 
	{
		return getCaseNumberField().getText();
	}
	
	public void emailFields(String from , String cc , String to, String subject , String body)
	{   
		new WebDriverWait(driver, 20).until(ExpectedConditions.visibilityOf(getEmailFromField()));
		
		Select fromField = new Select(getEmailFromField());
		fromField.selectByVisibleText(from);
		
		getEmailCCField().click();
		getEmailCCField().sendKeys(cc);
		
		getEmailToField().sendKeys(Keys.BACK_SPACE);
		getEmailToField().sendKeys(to);
		
		getEmailSubjectField().clear();
		getEmailSubjectField().sendKeys(subject);
		
		getEmailBodyField().sendKeys(Keys.TAB);
		Actions actionObj = new Actions(driver);
		actionObj.sendKeys(Keys.chord("A")).perform();
	}
	
	public void emailFieldsECAT(String from, String cc, String to, String subject, String body) 
	{

		Select fromField = new Select(getEmailFromField());
		fromField.selectByVisibleText(from);
		
		try 
		{
			getEmailAddCCField().click();
			getEmailAddCCField().sendKeys(cc);
		} 
		catch (ElementNotVisibleException e) 
		{
			getEmailCCField().click();
			getEmailCCField().sendKeys(cc);
		}
		
		getEmailToField().sendKeys(Keys.BACK_SPACE);
		getEmailToField().sendKeys(to);
		
		getEmailSubjectField().clear();
		getEmailSubjectField().sendKeys(subject);
		
		getEmailBodyField().sendKeys(Keys.TAB);
		Actions actionObj = new Actions(driver);
		actionObj.sendKeys(Keys.chord("A")).perform();
	}

	public void selectEmailTemplate(String templateFolder, String templateName) throws Exception
	{
		getEmailTemplateField().click();
		Thread.sleep(9000);
		WebElement folder = getTemplateFolderField();
		Select folderDropDown = new Select(folder);
		folderDropDown.selectByVisibleText(templateFolder);
		getTemplateNameField().click();
		Actions action = new Actions(driver);
		action.moveToElement(driver.findElement(By.xpath("//div[starts-with(text(),'" + templateName + "')]"))).click().build().perform();
	}
	
	public void caseStatusEmail(String caseStatusEmail) throws Exception
	{	
		Select statusCaseDropDown = new Select(getCaseStatusEmailField());
		statusCaseDropDown.selectByVisibleText(caseStatusEmail);
	}

	public String getLatestMailSentTime() throws ParseException
	{
		//System.out.println("The latest time " + getRelatedEmailMsgListField().getText());
		return getRelatedEmailMsgListField().getText();
	}

	public void clickSendEmail()
	{
		getSendEmailField().click();
	}
	
	public NewCasePage onClickNewButton()
	{
		getNewCaseBtnField().click();
		return new NewCasePage(driver);
	}
	
	public CaseEdit clickAcceptOpen() 
	{
		getAcceptOpenField().click();
		return new CaseEdit(driver);
	}

	public CaseEdit clickEdit() 
	{
		getCaseEditField().click();
		return new CaseEdit(driver);
	}
}

		