package com.macfee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.mcafee.framework.AutomationHelper;

/**
*
* @author Puneeth & Balamurugan
*
*/

public class NewCasePage 
{
	private WebDriver driver;
	private String sfExpTitle = "Select a record type for the new case";
	private By sfRecordTypeDDField=By.id("p3");
	private By sfContinueBtnField=By.xpath(".//input[@title = 'Continue']");
	
	public NewCasePage(WebDriver driver) 
	{
		this.driver = driver;
	}
	
	public WebElement getRecordTypeDDField()
	{
		return AutomationHelper.findElement(driver, sfRecordTypeDDField, 10);
	}
	
	public WebElement getContinueBtnField()
	{
		return AutomationHelper.findElement(driver, sfContinueBtnField, 10);
	}
	
	public void verifyInNewCasePage()
	{		
		Assert.assertEquals(getRecordTypeDDField().isDisplayed(), true , sfExpTitle);
	}
	
	public void selectCaseRecordType(String recordtype) throws Exception 
	{ 		
		Select dropdown= new Select(getRecordTypeDDField()); 
		dropdown.selectByVisibleText(recordtype); 
		System.out.println("Case Record Type is " + recordtype);
 	}
	 
	public CaseEdit onClickContinue()
	{
		getContinueBtnField().click();
		return new CaseEdit(driver);
	}
}
