package com.macfee.pages;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.macfee.utility.Constants;
import com.macfee.utility.GetData;
import com.mcafee.framework.AutomationHelper;

/**
*
* @author Puneeth & Balamurugan
*
*/

public class HomePage 
{
	
	private WebDriver driver;
	private By sfCaseTabField=By.id("Case_Tab");
	private By sfAccountTabField=By.id("Account_Tab");
	private By sfSearchTxtField=By.id("phSearchInput");
	private By sfSearchBtnField=By.id("phSearchButton");
	private By sfCaseStringField=By.xpath("//*[@id='Case_body']//table//tbody//tr[2]//th//a");
	private By sfCaseStatusField=By.xpath("//*[@id='Case_body']//table//tbody//tr[2]//td[3]");
	private By sfCaseSubjectField=By.xpath("//*[@id='Case_body']//table//tbody//tr[2]//td[2]");
	private By sfCaseOwnerField=By.xpath("//*[@id='Case_body']//table//tbody//tr[2]//td[5]//a");
	private By sfConsumerContactField=By.xpath("//div[@id='Account_body']//table//tbody//tr[2]//th//a");

	public HomePage(WebDriver driver) 
	{
		this.driver=driver;
	}
	
	public boolean isHomePageLaunched()
	{		
		return AutomationHelper.findElement(driver, sfCaseTabField, 15).isDisplayed();
	}
	
	public WebElement getSearchTxtFiled()
	{
		return AutomationHelper.findElement(driver, sfSearchTxtField, 10);
	}
	
	public WebElement getSearchBtnFiled()
	{
		return AutomationHelper.findElement(driver, sfSearchBtnField, 10);
	}
	
	public WebElement getCaseStringField()
	{
		return AutomationHelper.findElement(driver, sfCaseStringField, 10);
	}
	
	public WebElement getCaseStatusField()
	{
		return AutomationHelper.findElement(driver, sfCaseStatusField, 10);
	}
	
	public WebElement getCaseSubjectField()
	{
		return AutomationHelper.findElement(driver, sfCaseSubjectField, 10);
	}
	
	public WebElement getCaseOwnerField()
	{
		return AutomationHelper.findElement(driver, sfCaseOwnerField, 10);
	}
	
	public WebElement getConsumerContactField()
	{
		return AutomationHelper.findElement(driver, sfConsumerContactField, 10);
	}
	
	public Case onClickCaseTab()
	{
		AutomationHelper.findElement(driver, sfCaseTabField, 10).click();
		return new Case(driver);
	}
	
	public ConsumerContact clickConsumerContact()
	{
		AutomationHelper.findElement(driver, sfAccountTabField, 10).click();
		return new ConsumerContact(driver);
	}
	
	//Creating a date/time variable for appending while creating a unique items
	public String currentTimeVariable()
	{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH_mm");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now));
		return dtf.format(now);
	}
	
	public String globalSearch(String searchItem) throws InterruptedException
	{
		getSearchTxtFiled().sendKeys(searchItem);
		getSearchBtnFiled().click();
		Thread.sleep(3000);
		return searchItem;
	}
	
	public Case searchCasesTable()
	{
		String caseNumber = getCaseStringField().getText();
		System.out.println("Click on the case number = " + caseNumber);
		getCaseStringField().click();
		return new Case(driver);
	}
	
	public String caseStatus() throws InterruptedException
	{
		for(int i=0; i<5; i++)
		{
			Thread.sleep(10000);
			driver.navigate().refresh();
		}
		String caseStatus = getCaseStatusField().getText();
		return caseStatus;
	}
	
	public String caseSubject()
	{
		return getCaseSubjectField().getText();
	}
	
	public String caseOwner() throws Exception
	{
		Thread.sleep(2000);
		String caseOwner = getCaseOwnerField().getText();
		GetData.setExcelFile(Constants.Path_TestData + Constants.File_TestData, "Case Creation");
		GetData.setCellData(caseOwner, 1, 2);
		return caseOwner;
	}
	
	public ConsumerContact searchConsumerContacts() throws InterruptedException
	{
		String consumerContact = getConsumerContactField().getText();
		System.out.println("Click on the consumer number = " + consumerContact);
		getConsumerContactField().click();
		Thread.sleep(2000);
		return new ConsumerContact(driver);
	}
	
	public boolean searchIfContactExists(String contactName)
	{
		return getConsumerContactField().isDisplayed();
	}
}
