package com.macfee.pages;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.mcafee.framework.AutomationHelper;

/**
*
* @author Puneeth & Balamurugan
*
*/

public class ConsumerContact 
{
	WebDriver driver;
	private By sfNewConsumerContactBtnField=By.xpath(".//input[@title='New']");
	private By sfNewCaseContactField=By.xpath("//input[@title='New Case']");
	private By sfCreatedByField=By.id("CreatedBy_ileinner");
	private By sfConsumerStringField=By.xpath("//*[@id='contactHeaderRow']/div[2]/h2"); 
	
	public ConsumerContact(WebDriver driver) 
	{
		this.driver = driver;
	}
	
	public WebElement getNewCaseBtnField()
	{
		return AutomationHelper.findElement(driver, sfNewConsumerContactBtnField, 10);
	}
	
	public WebElement getNewCaseContactField()
	{
		return AutomationHelper.findElement(driver, sfNewCaseContactField, 10);
	}
	
	public WebElement getCreatedByField()
	{
		return AutomationHelper.findElement(driver, sfCreatedByField, 10);
	}
	
	public WebElement getConsumerStringField()
	{
		return AutomationHelper.findElement(driver, sfConsumerStringField, 10);
	}
	
	public String verifyConsumerCreated()
	{	
		String sfConsumerString = getConsumerStringField().getText();
		System.out.println("String with consumer contact " + sfConsumerString);
		Pattern intsOnly = Pattern.compile("\\d+");
		Matcher makeMatch = intsOnly.matcher(sfConsumerString);
		makeMatch.find();
		System.out.println(makeMatch.group() + " Consumer contact created in SF Application");
		return makeMatch.group();
	}
 
	public ConsumerContactEdit clickNewButton() throws InterruptedException
	{
		getNewCaseBtnField().click();
		return new ConsumerContactEdit(driver);
	}
 
	public String verifyConsumerContactPage()
	{
		String consumerContactTitle = driver.getTitle();
		Assert.assertEquals(consumerContactTitle.contains("Contact"), true, consumerContactTitle + "Page is open");
		return consumerContactTitle;
	}
	
	public boolean verifyPersonConsumerContact(String salutation, String firstName, String lastName)
	{
		Assert.assertEquals(driver.getTitle(), "Person Consumer Contact: " + salutation + " " + firstName + " " + lastName + " ~ Salesforce - Unlimited Edition", driver.getTitle() + "page is open");
		return true;
	}
	
	public String getCreatedBy()
	{
		return getCreatedByField().getText();
	}
	
	public NewCasePage clickNewCaseContact()
	{
		getNewCaseContactField().click();
		return new NewCasePage(driver);
	}
}
