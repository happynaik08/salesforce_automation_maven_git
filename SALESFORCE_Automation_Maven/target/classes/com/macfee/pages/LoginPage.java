package com.macfee.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.mcafee.framework.AutomationHelper;

/**
*
* @author Balamurugan
*
*/

public class LoginPage 
{
	private WebDriver driver;
	private By userNameField=By.id("username");
	private By passwordField=By.id("password");
	private By loginButton=By.id("Login");
	
	public LoginPage(WebDriver driver) 
	{	
		this.driver=driver;	
	}
	
	public WebElement getUserNameField()
	{
		return AutomationHelper.findElement(driver, userNameField, 30);
	}
	
	public WebElement getpasswordField()
	{
		return AutomationHelper.findElement(driver, passwordField, 30);
	}
	
	public WebElement getloginButton()
	{
		return AutomationHelper.findElement(driver, loginButton, 30);
	}
	
	public HomePage doLogIn(String userName,String password)
	{
		getUserNameField().sendKeys(userName);
		getpasswordField().sendKeys(password);
		getloginButton().click();
		return new HomePage(driver);
	}
}