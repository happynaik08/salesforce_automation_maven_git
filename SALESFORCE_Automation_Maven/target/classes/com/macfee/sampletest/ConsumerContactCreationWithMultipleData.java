package com.macfee.sampletest;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.macfee.pages.ConsumerContact;
import com.macfee.pages.HomePage;
import com.macfee.pages.LoginPage;
import com.macfee.pages.ConsumerContactEdit;
import com.mcafee.framework.BaseClass;
import com.mcafee.framework.ConfigData;
import com.mcafee.framework.DataProviderClass;

/**
*
* @author Balamurugan
*
*/

public class ConsumerContactCreationWithMultipleData extends BaseClass
{
	private static LoginPage loginPage;
	private static HomePage home;
	private static ConsumerContact consumerContactTab;
	private static ConsumerContactEdit newConsumer;
	private static DataProviderClass dataproviders;
	static int i;
	
	@Override
	@BeforeClass
	public void setup() 
	{
		super.setup();
		i=1;
		System.out.println("Setup done");
		loginPage=commonMethods.launchSFApplication();
		home=loginPage.doLogIn(ConfigData.SFUSERNAME, ConfigData.SFPASSWORD);
		Assert.assertTrue(home.isHomePageLaunched(), "SF home page launched successfully");
		System.out.println("Consumer Contact Creator");
	}
	
	@Test(dataProvider="sfConsumerContactCreator",dataProviderClass=DataProviderClass.class)
	public void test(String...consumerContactTestData) throws Exception
	{	
		consumerContactTab=home.clickConsumerContact();
		newConsumer=consumerContactTab.clickNewButton();
		newConsumer.selectSalutation(consumerContactTestData[0]);
		newConsumer.putFirstName(consumerContactTestData[1]);
		newConsumer.putLastName(consumerContactTestData[2]);
		newConsumer.putPrimaryEmailID(consumerContactTestData[3]);
		newConsumer.putPrimaryPhNo(consumerContactTestData[4]);
		newConsumer.selectCustLanguage(consumerContactTestData[5]);
		newConsumer.selectCustCountry(consumerContactTestData[6]);		
		newConsumer.clickSave();
		//String consumername=ConsumerContact.verifyConsumerCreated();
		//Assert.assertNotNull(consumername, "Consumer Created");
		//dataproviders.writeDataToExcel("./resources/TestData.xlsx", "SFConsumerContactCreations", consumername, i, consumerContactTestData.length);
	}
	
	@AfterMethod
	public void colno()
    {
		i++;
	}
}
