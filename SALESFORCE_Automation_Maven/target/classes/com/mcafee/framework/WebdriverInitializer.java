package com.mcafee.framework;

import java.io.File;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

/**
*
* @author Balamurugan
*
*/

public class WebdriverInitializer 
{
	private String browser;
	public WebdriverInitializer(String browser) 
	{
		this.browser=browser;
	}
	
	protected WebDriver getWebDriver()
	{
		return webdriverInitializer(this.browser);
	}

	protected static WebDriver webDriver = null;

	private  WebDriver webdriverInitializer(String type) 
	{
		switch (type) 
		{
			case "chrome":
				File chromeFile = new File(".\\driver\\chromedriver.exe");
				System.setProperty("webdriver.chrome.driver", chromeFile.getAbsolutePath());
				webDriver = new ChromeDriver();
				webDriver.manage().window().maximize();

			case "firefox":
				break;
				
			case "ie":
				File ieFile = new File(".\\driver\\IEDriverServer.exe");
				System.setProperty("webdriver.ie.driver", ieFile.getAbsolutePath());
				webDriver = new InternetExplorerDriver();
				webDriver.manage().window().maximize();
				break;
				
			default:
				break;
		}
		return webDriver;
	}

	public void closeDriver() 
	{
		if (webDriver != null) 
		{
			this.webDriver.close();
			this.webDriver.quit();
		}
	}
}
